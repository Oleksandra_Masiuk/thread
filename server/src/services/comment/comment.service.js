class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  updateComment(commentId, comment) {
    return this._commentRepository.updateById(commentId, { ...comment });
  }

  async deleteComment(commentId) {
    const comment = await this._commentRepository.getCommentById(commentId);
    await this._commentRepository.softDeleteById(commentId);
    return comment;
  }

  async getCommentWithLikesById(commentId, isLike) {
    const comment = await this._commentRepository.getCommentById(commentId);
    const reactions = await this._commentReactionRepository.getCommentReactions(
      commentId,
      isLike
    );
    return { ...comment[0], reactions };
  }

  async setLike(userId, { commentId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({
        userId,
        commentId,
        isLike
      });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }

  async setDislike(userId, { commentId, isLike = false }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({
        userId,
        commentId,
        isLike
      });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }
}

export { Comment };
