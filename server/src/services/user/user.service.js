class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  updateUser(userId, user) {
    return this._userRepository.updateById(userId, { ...user });
  }

  async getUserByUsername(username) {
    const user = await this._userRepository.getByUsername(username);

    if (!user) {
      return {};
    }
    return user;
  }

  async getUserByEmail(email) {
    const user = await this._userRepository.getByEmail(email);
    if (!user) {
      return {};
    }
    return user;
  }
}

export { User };
