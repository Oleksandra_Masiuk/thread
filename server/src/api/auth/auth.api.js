import { AuthApiPath } from '../../common/enums/enums';
import { getErrorStatusCode } from '../../helpers/http/get-status-code.helper';

const initAuth = (router, opts, done) => {
  const { auth: authService, user: userService } = opts.services;

  // user added to the request (req.user) in auth plugin, authorization.plugin.js
  router
    .post(AuthApiPath.LOGIN, async (req, res) => {
      try {
        const user = await authService.verifyLoginCredentials(req.body);
        return await authService.login(user);
      } catch (err) {
        return res.status(getErrorStatusCode(err)).send(err);
      }
    })
    .put(AuthApiPath.PASSWORD, async (req, res) => {
      try {
        const user = await authService.changePassword(req.body);
        return user;
      } catch (err) {
        return res.status(getErrorStatusCode(err)).send(err);
      }
    })
    .post(AuthApiPath.REGISTER, async (req, res) => {
      try {
        return await authService.register(req.body);
      } catch (err) {
        return res.status(getErrorStatusCode(err)).send(err);
      }
    })
    .put(AuthApiPath.USER + AuthApiPath.$ID, async req => {
      const user = await userService.updateUser(req.params.id, req.body);
      req.io.emit('update_profile', user); // notify user that his profile was updated
      return user;
    })
    .get(
      AuthApiPath.USER + AuthApiPath.USERNAME + AuthApiPath.$USERNAME,
      async req => {
        const user = await userService.getUserByUsername(req.params.username);
        return user;
      }
    )
    .get(
      AuthApiPath.USER + AuthApiPath.EMAIL + AuthApiPath.$EMAIL,
      async req => {
        const user = await userService.getUserByEmail(req.params.email);
        return user;
      }
    )
    .get(AuthApiPath.USER + AuthApiPath.$ID, req => userService.getUserById(req.params.id))
    .get(AuthApiPath.USER, req => userService.getUserById(req.user.id));

  done();
};

export { initAuth };
