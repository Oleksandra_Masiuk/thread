import { PostsApiPath } from '../../common/enums/enums';

const initPost = (router, opts, done) => {
  const { post: postService } = opts.services;

  router
    .get(PostsApiPath.ROOT, req => postService.getPosts(req.query))
    .get(PostsApiPath.$ID, req => postService.getPostById(req.params.id))
    .get(PostsApiPath.REACTIONS_LIKES + PostsApiPath.$ID, req => postService.getPostWithLikesById(req.params.id, true))
    .get(PostsApiPath.REACTIONS_DISLIKES + PostsApiPath.$ID, req => {
      const { id } = req.params;
      return postService.getPostWithLikesById(id, false);
    })
    .post(PostsApiPath.ROOT, async req => {
      const post = await postService.create(req.user.id, req.body);
      req.io.emit('new_post', post); // notify all users that a new post was created
      return post;
    })
    .put(PostsApiPath.$ID, async req => {
      const post = await postService.updatePost(req.params.id, req.body);
      req.io.emit('update_post', post); // notify all users that a post was updated
      return post;
    })
    .delete(PostsApiPath.$ID, async req => {
      const post = await postService.deletePost(req.params.id);
      req.io.emit('delete_post', post); // notify all users that a post was deleted
      return post;
    })
    .put(PostsApiPath.LIKE, async req => {
      const reaction = await postService.setLike(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        req.io.to(reaction.post.userId).emit('like', 'Your post was liked!', req.user.username, reaction.post.id);
      }
      return reaction;
    })
    .put(PostsApiPath.DISLIKE, async req => {
      const reaction = await postService.setDislike(req.user.id, req.body);

      if (reaction.post && reaction.post.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his post
        req.io
          .to(reaction.post.userId)
          .emit('dislike', 'Your post was disliked!', req.user.username, reaction.post.id);
      }
      return reaction;
    });

  done();
};

export { initPost };
