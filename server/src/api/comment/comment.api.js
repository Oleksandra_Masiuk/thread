import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (router, opts, done) => {
  const { comment: commentService } = opts.services;

  router
    .get(CommentsApiPath.$ID, req => commentService.getCommentById(req.params.id))
    .get(CommentsApiPath.REACTIONS_LIKES + CommentsApiPath.$ID, req => {
      const { id } = req.params;
      return commentService.getCommentWithLikesById(id, true);
    })
    .get(CommentsApiPath.REACTIONS_DISLIKES + CommentsApiPath.$ID, req => {
      const { id } = req.params;
      return commentService.getCommentWithLikesById(id, false);
    })
    .post(CommentsApiPath.ROOT, req => commentService.create(req.user.id, req.body))
    .put(CommentsApiPath.$ID, async req => {
      const comment = await commentService.updateComment(
        req.params.id,
        req.body
      );
      req.io.emit('update_comment', comment); // notify all users that a comment was updated
      return comment;
    })
    .delete(CommentsApiPath.$ID, async req => {
      const comment = await commentService.deleteComment(req.params.id);
      req.io.emit('delete_comment', comment); // notify all users that a comment was deleted
      return comment;
    })
    .put(CommentsApiPath.LIKE, async req => {
      const reaction = await commentService.setLike(req.user.id, req.body);

      if (reaction.comment && reaction.comment.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his comment
        req.io
          .to(reaction.comment.userId)
          .emit('like_comment', 'Your comment was liked!');
      }
      return reaction;
    })
    .put(CommentsApiPath.DISLIKE, async req => {
      const reaction = await commentService.setDislike(req.user.id, req.body);

      if (reaction.comment && reaction.comment.userId !== req.user.id) {
        // notify a user if someone (not himself) liked his comment
        req.io
          .to(reaction.comment.userId)
          .emit('dislike_comment', 'Your comment was disliked!');
      }
      return reaction;
    });
  done();
};

export { initComment };
