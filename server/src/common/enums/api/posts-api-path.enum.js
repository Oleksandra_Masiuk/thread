const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  LIKE: '/like',
  DISLIKE: '/dislike',
  REACTIONS_LIKES: '/reactions/likes',
  REACTIONS_DISLIKES: '/reactions/dislikes'
};

export { PostsApiPath };
