const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  $ID: '/:id',
  USERNAME: '/username',
  PASSWORD: '/password',
  $USERNAME: '/:username',
  EMAIL: '/email',
  $EMAIL: '/:email'
};

export { AuthApiPath };
