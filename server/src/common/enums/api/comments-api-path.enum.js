const CommentsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACTIONS_LIKES: '/reactions/likes',
  REACTIONS_DISLIKES: '/reactions/dislikes',
  LIKE: '/like',
  DISLIKE: '/dislike'
};

export { CommentsApiPath };
