import { ApiPath, AuthApiPath, ENV } from '../enums/enums';

const WHITE_ROUTES = [
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.LOGIN}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.REGISTER}`,
  `${ENV.APP.API_PATH}${ApiPath.AUTH}${AuthApiPath.PASSWORD}`
];

export { WHITE_ROUTES };
