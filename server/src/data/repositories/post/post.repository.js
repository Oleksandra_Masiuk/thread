import { Abstract } from '../abstract/abstract.repository';
import { CommentModel } from '../../models';
import {
  getCommentsCountQuery,
  getReactionsQuery,
  getWhereUserIdQuery,
  getCommentReactionsQuery
} from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      isOwnPosts,
      isLikedPosts,
      userId
    } = filter;
    if (userId) {
      if (isLikedPosts.toString() === 'true') {
        return this.model
          .query()
          .select(
            'posts.*',
            getCommentsCountQuery(this.model),
            getReactionsQuery(this.model)(true),
            getReactionsQuery(this.model)(false)
          )
          .whereNull('deletedAt')
          .withGraphFetched('[image, user.image]')
          .orderBy('updatedAt', 'desc')
          .offset(offset)
          .limit(limit)
          .join('post_reactions', 'posts.id', '=', 'post_reactions.post_id')
          .distinct('posts.id')
          .where('isLike', '=', true)
          .andWhere('post_reactions.userId', '=', userId);
      }
      if (isOwnPosts.toString() === 'false') {
        return this.model
          .query()
          .select(
            'posts.*',
            getCommentsCountQuery(this.model),
            getReactionsQuery(this.model)(true),
            getReactionsQuery(this.model)(false)
          )
          .whereNot('userId', '=', userId)
          .whereNull('deletedAt')
          .withGraphFetched('[image, user.image]')
          .orderBy('updatedAt', 'desc')
          .offset(offset)
          .limit(limit);
      }
    }
    return this.model
      .query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereUserIdQuery(userId))
      .whereNull('deletedAt')
      .withGraphFetched('[image, user.image]')
      .orderBy('updatedAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model
      .query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched(
        '[comments(onlyNotDeleted).user.image, user.image, image]'
      )
      .modifiers({
        onlyNotDeleted(builder) {
          builder.whereNull('deletedAt');
        }
      })
      .modifyGraph('comments', builder => {
        builder.select(
          'comments.*',
          getCommentReactionsQuery(CommentModel)(true),
          getCommentReactionsQuery(CommentModel)(false)
        );
      })
      .first();
  }

  updateById(id, data) {
    return this.model
      .query()
      .patchAndFetchById(id, { image_id: data.imageId, body: data.body });
  }
}

export { Post };
