const getCommentsCountQuery = model => model
  .relatedQuery('comments')
  .count()
  .whereNull('deletedAt')
  .as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions').count().where({ isLike }).as(col);
};

const getWhereUserIdQuery = userId => builder => {
  if (userId) {
    builder.where({ userId });
  }
};

const getCommentReactionsQuery = model => isLike => {
  const col = isLike ? 'commentLikeCount' : 'commentDislikeCount';

  return model
    .relatedQuery('commentReactions')
    .count()
    .where({ isLike })
    .as(col);
};

export {
  getCommentsCountQuery,
  getReactionsQuery,
  getWhereUserIdQuery,
  getCommentReactionsQuery
};
