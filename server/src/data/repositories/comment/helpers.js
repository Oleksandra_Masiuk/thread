const getReactionsQuery = model => isLike => {
  const col = isLike ? 'commentLikeCount' : 'commentDislikeCount';

  return model
    .relatedQuery('commentReactions')
    .count()
    .where({ isLike })
    .as(col);
};

export { getReactionsQuery };
