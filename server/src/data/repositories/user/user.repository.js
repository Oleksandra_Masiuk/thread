import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor({ userModel }) {
    super(userModel);
  }

  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.query().select().where({ email }).first();
  }

  getByUsername(username) {
    return this.model.query().select().where({ username }).first();
  }

  getUserById(id) {
    return this.model
      .query()
      .select('id', 'createdAt', 'email', 'updatedAt', 'username', 'status')
      .where({ id })
      .withGraphFetched('[image]')
      .first();
  }

  updateById(id, data) {
    const { imageId, username, email, status } = data;
    return this.model.query().patchAndFetchById(id, {
      imageId,
      username,
      email,
      status
    });
  }

  async changePassword(username, password) {
    await this.model.query().patch({ password }).where({ username });
    const result = await this.getByUsername(username);
    return result;
  }
}

export { User };
