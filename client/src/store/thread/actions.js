import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();
    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const setCommentToUpdate = createAsyncThunk(
  ActionType.SET_COMMENT_TO_UPDATE,
  async (commentId, { extra: { services } }) => {
    let comment;
    if (commentId) {
      const comm = await services.comment.getComment(commentId);
      [comment] = comm;
    }
    return { comment };
  }
);

const setPostToUpdate = createAsyncThunk(
  ActionType.SET_POST_TO_UPDATE,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const applyUpdatePost = createAsyncThunk(
  ActionType.APPLY_UPDATE_POST,
  async (postId, { getState, extra: { services } }) => {
    const {
      posts: { posts, expandedPost }
    } = getState();
    const newPost = await services.post.getPost(postId);
    const newPosts = posts.map(post => (post.id !== postId ? post : newPost));
    const newExpandedPost = expandedPost?.id !== postId ? expandedPost : newPost;
    return { newPosts, newExpandedPost };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const updatePost = createAsyncThunk(
  ActionType.UPDATE_POST,
  async (post, { extra: { services } }) => {
    await services.post.updatePost(post, post.id);
    const newPost = await services.post.getPost(post.id);
    return { newPost };
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.deletePost(postId);
    return { id: post.id };
  }
);

const deleteComment = createAsyncThunk(
  ActionType.DELETE_COMMENT,
  async (commentId, { getState, extra: { services } }) => {
    const comm = await services.comment.deleteComment(commentId);
    const comment = comm[0];

    const mapComments = post => {
      const comments = post.comments.filter(com => com.id !== commentId);
      return {
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments
      };
    };
    const {
      posts: { expandedPost }
    } = getState();

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return {
      id: comment.id,
      expandedPost: updatedExpandedPost
    };
  }
);

const applyDeletePost = createAsyncThunk(
  ActionType.DELETE_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { id: post.id };
  }
);

const toggleExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const likePost = createAsyncThunk(
  ActionType.LIKE,
  async (postId, { getState, extra: { services } }) => {
    const { id, updatedAt, createdAt } = await services.post.likePost(postId);
    const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

    const mapLikes = post => {
      if (updatedAt === createdAt) {
        return {
          ...post,
          likeCount: Number(post.likeCount) + diff
        };
      }
      return {
        ...post,
        likeCount: Number(post.likeCount) + diff,
        dislikeCount: Number(post.dislikeCount) - diff // diff is taken from the current closure
      };
    };

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));
    const updatedExpandedPost = expandedPost?.id === postId ? mapLikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.DISLIKE,
  async (postId, { getState, extra: { services } }) => {
    const { id, updatedAt, createdAt } = await services.post.dislikePost(
      postId
    );
    const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - like was added

    const mapDislikes = post => {
      if (updatedAt === createdAt) {
        return {
          ...post,
          dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
        };
      }
      return {
        ...post,
        likeCount: Number(post.likeCount) - diff,
        dislikeCount: Number(post.dislikeCount) + diff // diff is taken from the current closure
      };
    };

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));
    const updatedExpandedPost = expandedPost?.id === postId ? mapDislikes(expandedPost) : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const likeComment = createAsyncThunk(
  ActionType.LIKE_COMMENT,
  async (commentId, { getState, extra: { services } }) => {
    const { id, updatedAt, createdAt } = await services.comment.likeComment(
      commentId
    );

    const diff = id ? 1 : -1; // if ID exists then the comment was liked, otherwise - like was removed

    const mapLikes = comment => {
      if (updatedAt === createdAt) {
        return {
          ...comment,
          commentLikeCount: Number(comment.commentLikeCount) + diff
        };
      }
      return {
        ...comment,
        commentLikeCount: Number(comment.commentLikeCount) + diff,
        commentDislikeCount: Number(comment.commentDislikeCount) - diff // diff is taken from the current closure
      };
    };

    const {
      posts: { expandedPost }
    } = getState();

    const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));

    return { expandedPost: { ...expandedPost, comments: updated } };
  }
);

const dislikeComment = createAsyncThunk(
  ActionType.DISLIKE_COMMENT,
  async (commentId, { getState, extra: { services } }) => {
    const { id, updatedAt, createdAt } = await services.comment.dislikeComment(
      commentId
    );
    const diff = id ? 1 : -1; // if ID exists then the comment was disliked, otherwise - like was added

    const mapDislikes = comment => {
      if (updatedAt === createdAt) {
        return {
          ...comment,
          commentDislikeCount: Number(comment.commentDislikeCount) + diff
        };
      }
      return {
        ...comment,
        commentLikeCount: Number(comment.commentLikeCount) - diff,
        commentDislikeCount: Number(comment.commentDislikeCount) + diff // diff is taken from the current closure
      };
    };

    const {
      posts: { expandedPost }
    } = getState();

    const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));

    return { expandedPost: { ...expandedPost, comments: updated } };
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comm = await services.comment.getComment(id);
    const comment = comm[0];

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const updateComment = createAsyncThunk(
  ActionType.UPDATE_COMMENT,
  async (comment, { getState, extra: { services } }) => {
    await services.comment.updateComment(comment, comment.id);
    const newCom = await services.comment.getComment(comment.id);
    const newComment = newCom[0];
    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount),
      comments: [
        ...(post.comments.filter(comm => comm.id !== comment.id) || []),
        newComment
      ] // comment is taken from the current closure
    });

    const {
      posts: { expandedPost }
    } = getState();

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { expandedPost: updatedExpandedPost };
  }
);

const setPostToLike = createAsyncThunk(
  ActionType.SET_POST_TO_LIKE,
  async (postId, { extra: { services } }) => {
    const post = postId
      ? await services.post.getPostWithLikes(postId)
      : undefined;
    return { post };
  }
);

const setPostToDislike = createAsyncThunk(
  ActionType.SET_POST_TO_DISLIKE,
  async (postId, { extra: { services } }) => {
    const post = postId
      ? await services.post.getPostWithDislikes(postId)
      : undefined;
    return { post };
  }
);

const setCommentToLike = createAsyncThunk(
  ActionType.SET_COMMENT_TO_LIKE,
  async (commentId, { extra: { services } }) => {
    const comment = commentId
      ? await services.comment.getCommentWithLikes(commentId)
      : undefined;
    return { comment };
  }
);

const setCommentToDislike = createAsyncThunk(
  ActionType.SET_COMMENT_TO_DISLIKE,
  async (commentId, { extra: { services } }) => {
    const comment = commentId
      ? await services.comment.getCommentWithDislikes(commentId)
      : undefined;
    return { comment };
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  updatePost,
  deletePost,
  applyDeletePost,
  setPostToUpdate,
  setPostToLike,
  setCommentToUpdate,
  deleteComment,
  updateComment,
  likeComment,
  dislikeComment,
  setCommentToLike,
  setPostToDislike,
  setCommentToDislike,
  applyUpdatePost
};
