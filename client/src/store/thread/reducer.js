import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as threadActions from './actions';

const initialState = {
  posts: [],
  expandedPost: null,
  hasMorePosts: true,
  postToUpdate: null,
  commentToUpdate: null,
  commentToLike: null,
  commentToDislike: null,
  postToLike: null,
  postToDislike: null
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(threadActions.loadPosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = posts;
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.loadMorePosts.pending, state => {
    state.hasMorePosts = null;
  });
  builder.addCase(threadActions.loadMorePosts.fulfilled, (state, action) => {
    const { posts } = action.payload;

    state.posts = state.posts.concat(posts);
    state.hasMorePosts = Boolean(posts.length);
  });
  builder.addCase(threadActions.setPostToLike.fulfilled, (state, action) => {
    const { post } = action.payload;
    state.postToLike = post;
  });
  builder.addCase(threadActions.setPostToDislike.fulfilled, (state, action) => {
    const { post } = action.payload;
    state.postToDislike = post;
  });
  builder.addCase(threadActions.setCommentToLike.fulfilled, (state, action) => {
    const { comment } = action.payload;
    state.commentToLike = comment;
  });
  builder.addCase(
    threadActions.setCommentToDislike.fulfilled,
    (state, action) => {
      const { comment } = action.payload;
      state.commentToDislike = comment;
    }
  );
  builder.addCase(
    threadActions.toggleExpandedPost.fulfilled,
    (state, action) => {
      const { post } = action.payload;
      state.expandedPost = post;
    }
  );
  builder.addCase(threadActions.setPostToUpdate.fulfilled, (state, action) => {
    const { post } = action.payload;
    state.postToUpdate = post;
  });
  builder.addCase(
    threadActions.setCommentToUpdate.fulfilled,
    (state, action) => {
      const { comment } = action.payload;
      state.commentToUpdate = comment;
    }
  );
  builder.addCase(threadActions.updatePost.fulfilled, (state, action) => {
    const { newPost } = action.payload;
    state.posts = state.posts.filter(statePost => statePost.id !== newPost.id);
    state.posts = [newPost, ...state.posts];
    if (state.expandedPost?.id === newPost.id) {
      state.expandedPost = newPost;
    }
  });
  builder.addCase(threadActions.applyUpdatePost.fulfilled, (state, action) => {
    const { newPosts, newExpandedPost } = action.payload;
    state.posts = newPosts;
    state.expandedPost = newExpandedPost;
  });
  builder.addCase(threadActions.updateComment.fulfilled, (state, action) => {
    const { expandedPost } = action.payload;
    state.expandedPost = expandedPost;
    state.commentToUpdate = null;
  });
  builder.addMatcher(
    isAnyOf(
      threadActions.deletePost.fulfilled,
      threadActions.applyDeletePost.fulfilled
    ),
    (state, action) => {
      const { id } = action.payload;
      state.posts = state.posts.filter(post => post.id !== id);
      state.expandedPost = null;
    }
  );
  builder.addMatcher(
    isAnyOf(threadActions.deleteComment.fulfilled),
    (state, action) => {
      const { expandedPost } = action.payload;
      state.expandedPost = expandedPost;
      state.posts = state.posts.map(post => {
        if (post.id === expandedPost.id) {
          return {
            ...post,
            commentCount: expandedPost.commentCount
          };
        }
        return post;
      });
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.likePost.fulfilled,
      threadActions.dislikePost.fulfilled,
      threadActions.addComment.fulfilled
    ),
    (state, action) => {
      const { posts, expandedPost } = action.payload;
      state.posts = posts;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.likeComment.fulfilled,
      threadActions.dislikeComment.fulfilled
    ),
    (state, action) => {
      const { expandedPost } = action.payload;
      state.expandedPost = expandedPost;
    }
  );
  builder.addMatcher(
    isAnyOf(
      threadActions.applyPost.fulfilled,
      threadActions.createPost.fulfilled
    ),
    (state, action) => {
      const { post } = action.payload;
      state.posts = [post, ...state.posts];
    }
  );
});

export { reducer };
