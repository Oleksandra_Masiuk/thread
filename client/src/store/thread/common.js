const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  LIKE: 'thread/like',
  DISLIKE: 'thread/dislike',
  DELETE_POST: 'thread/delete-post',
  UPDATE_POST: 'thread/update-post',
  APPLY_UPDATE_POST: 'thread/apply-update-post',
  COMMENT: 'thread/comment',
  SET_POST_TO_UPDATE: 'thread/set-post-to-update',
  SET_POST_TO_LIKE: 'thread/set-post-to-like',
  SET_POST_TO_DISLIKE: 'thread/set-post-to-dislike',
  DELETE_COMMENT: 'thread/delete-comment',
  UPDATE_COMMENT: 'thread/update-comment',
  SET_COMMENT_TO_UPDATE: 'thread/set-comment-to-update',
  LIKE_COMMENT: 'thread/like_comment',
  DISLIKE_COMMENT: 'thread/dislike_comment',
  SET_COMMENT_TO_LIKE: 'thread/set-comment-to-like',
  SET_COMMENT_TO_DISLIKE: 'thread/set-comment-to-dislike'
};

export { ActionType };
