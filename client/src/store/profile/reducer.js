import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import * as profileActions from './actions';

const initialState = {
  user: null,
  userToUpdate: null
};

const reducer = createReducer(initialState, builder => {
  builder.addCase(
    profileActions.setProfileToUpdate.fulfilled,
    (state, action) => {
      const { user } = action.payload;
      state.userToUpdate = user;
    }
  );
  builder.addCase(profileActions.updateProfile.fulfilled, (state, action) => {
    const { user } = action.payload;
    state.user = user;
    state.userToUpdate = undefined;
  });
  builder.addMatcher(
    isAnyOf(
      profileActions.login.fulfilled,
      profileActions.logout.fulfilled,
      profileActions.register.fulfilled,
      profileActions.loadCurrentUser.fulfilled,
      profileActions.changePassword.fulfilled
    ),
    (state, action) => {
      state.user = action.payload;
    }
  );
  builder.addMatcher(
    isAnyOf(
      profileActions.login.rejected,
      profileActions.logout.rejected,
      profileActions.register.rejected,
      profileActions.loadCurrentUser.rejected
    ),
    state => {
      state.user = null;
    }
  );
});

export { reducer };
