const ActionType = {
  LOG_IN: 'profile/log-in',
  LOG_OUT: 'profile/log-out',
  REGISTER: 'profile/register',
  UPDATE_PROFILE: 'profile/update-profile',
  SET_PROFILE_TO_UPDATE: 'profile/set-profile-to-update',
  CHANGE_PASSWORD: 'profile/change-password'
};

export { ActionType };
