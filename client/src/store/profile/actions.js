import { createAsyncThunk } from '@reduxjs/toolkit';
import { HttpError } from 'exceptions/exceptions';
import { HttpCode, StorageKey, ExceptionMessage } from 'common/enums/enums';

import * as servs from 'services/services';

import { ActionType } from './common';

const login = createAsyncThunk(
  ActionType.LOG_IN,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.login(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const register = createAsyncThunk(
  ActionType.REGISTER,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.registration(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const logout = createAsyncThunk(
  ActionType.LOG_OUT,
  (_request, { extra: { services } }) => {
    services.storage.removeItem(StorageKey.TOKEN);

    return null;
  }
);

const loadCurrentUser = createAsyncThunk(
  ActionType.LOG_IN,
  async (_request, { dispatch, rejectWithValue, extra: { services } }) => {
    try {
      return await services.auth.getCurrentUser();
    } catch (err) {
      const isHttpError = err instanceof HttpError;

      if (isHttpError && err.status === HttpCode.UNAUTHORIZED) {
        dispatch(logout());
      }

      return rejectWithValue(err?.message ?? ExceptionMessage.UNKNOWN_ERROR);
    }
  }
);

const setProfileToUpdate = createAsyncThunk(
  ActionType.SET_PROFILE_TO_UPDATE,
  async (profileId, { extra: { services } }) => {
    const user = profileId ? await services.auth.getUser(profileId) : undefined;
    return { user };
  }
);

const updateProfile = createAsyncThunk(
  ActionType.UPDATE_PROFILE,
  async (profile, { extra: { services } }) => {
    await services.auth.updateUser(profile, profile.id);
    const user = await services.auth.getUser(profile.id);
    return { user };
  }
);

const checkIfUsernameExists = async username => {
  const user = await servs.auth.getUserByUsername(username);
  if (Object.keys(user).length === 0) {
    return false;
  }
  return true;
};

const changePassword = createAsyncThunk(
  ActionType.CHANGE_PASSWORD,
  async (registerPayload, { extra: { services } }) => {
    await services.auth.changePassword(registerPayload);
    return null;
  }
);

const checkIfEmailExists = async email => {
  const user = await servs.auth.getUserByEmail(email);
  if (Object.keys(user).length === 0) {
    return false;
  }
  return true;
};

export {
  login,
  register,
  logout,
  loadCurrentUser,
  setProfileToUpdate,
  updateProfile,
  changePassword,
  checkIfUsernameExists,
  checkIfEmailExists
};
