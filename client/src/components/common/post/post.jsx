import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { getFromNowTime } from 'helpers/helpers';
import {
  IconName,
  ButtonType,
  ButtonColor,
  ButtonSize
} from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import {
  IconButton,
  Image,
  Button,
  Modal,
  UserList
} from 'components/common/common';
import { UpdatePost } from 'components/thread/components/components';
import styles from './styles.module.scss';

const Post = ({
  post,
  onPostLike,
  onPostUpdate,
  onPostDelete,
  onPostDislike,
  onExpandedPostToggle,
  sharePost,
  onPostToUpdateToggle,
  uploadImage,
  onLikedPostToggle,
  onDislikedPostToggle
}) => {
  const { userId, postToUpdate, postToLike, postToDislike } = useSelector(
    state => ({
      userId: state.profile.user.id,
      postToUpdate: state.posts.postToUpdate,
      postToLike: state.posts.postToLike,
      postToDislike: state.posts.postToDislike
    })
  );

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const date = getFromNowTime(updatedAt);

  const isUpdating = postToUpdate && id === postToUpdate.id;
  const isOwn = userId === user.id;

  const handlePostLike = () => {
    onPostLike(id);
  };
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostToUpdate = () => onPostToUpdateToggle(id);
  const handlePostDelete = () => onPostDelete(id);
  const handlePostToLike = () => onLikedPostToggle(id);
  const handlePostToDislike = () => onDislikedPostToggle(id);

  const likes = Number(likeCount);
  const dislikes = Number(dislikeCount);
  const comments = Number(commentCount);

  return (
    <div className={styles.card}>
      {!isUpdating && image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>
            {' '}
            {updatedAt === createdAt ? 'posted by' : 'updated by'}
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </div>
        {isUpdating ? (
          <UpdatePost
            uploadImage={uploadImage}
            post={post}
            close={() => {
              onPostToUpdateToggle(undefined);
            }}
            onPostUpdate={onPostUpdate}
          />
        ) : (
          <>
            <p className={styles.description}>{body}</p>
            {isOwn && (
              <div className={styles.buttons}>
                <Button
                  color={ButtonColor.TEAL}
                  type={ButtonType.BUTTON}
                  onClick={handlePostToUpdate}
                >
                  Update
                </Button>
                <Button
                  color={ButtonColor.RED}
                  type={ButtonType.BUTTON}
                  onClick={handlePostDelete}
                >
                  Delete
                </Button>
              </div>
            )}
          </>
        )}
      </div>
      {likes + dislikes + comments !== 0 && (
        <div className={styles.extra}>
          {likes !== 0 && (
            <Button
              size={ButtonSize.MEDIUM}
              color={ButtonColor.TRANSPARENT}
              type={ButtonType.BUTTON}
              onClick={handlePostToLike}
            >
              {likeCount}
              <span className={styles.count}>
                {likes === 1 ? 'Like' : 'Likes'}
              </span>
            </Button>
          )}
          {dislikes !== 0 && (
            <Button
              size={ButtonSize.MEDIUM}
              color={ButtonColor.TRANSPARENT}
              type={ButtonType.BUTTON}
              onClick={handlePostToDislike}
            >
              {dislikeCount}
              <span className={styles.count}>
                {dislikes === 1 ? 'Dislike' : 'Dislikes'}
              </span>
            </Button>
          )}
          {comments !== 0 && (
            <Button
              size={ButtonSize.MEDIUM}
              color={ButtonColor.TRANSPARENT}
              type={ButtonType.BUTTON}
              onClick={handleExpandedPostToggle}
            >
              {commentCount}
              <span className={styles.count}>
                {comments === 1 ? 'Comment' : 'Comments'}
              </span>
            </Button>
          )}
        </div>
      )}
      <div className={styles.extra}>
        <IconButton iconName={IconName.THUMBS_UP} onClick={handlePostLike} />
        <IconButton
          iconName={IconName.THUMBS_DOWN}
          onClick={handlePostDislike}
        />
        <IconButton
          iconName={IconName.COMMENT}
          onClick={handleExpandedPostToggle}
        />
        <IconButton
          iconName={IconName.SHARE_ALTERNATE}
          onClick={() => sharePost(id)}
        />
      </div>
      {postToLike && (
        <Modal
          size="small"
          isOpen
          isCentered
          onClose={() => {
            onLikedPostToggle(undefined);
          }}
        >
          <UserList reactions={postToLike.reactions} title="Liked by" />
        </Modal>
      )}
      {postToDislike && (
        <Modal
          size="small"
          isOpen
          isCentered
          onClose={() => {
            onDislikedPostToggle(undefined);
          }}
        >
          <UserList reactions={postToDislike.reactions} title="Disliked by" />
        </Modal>
      )}
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onPostToUpdateToggle: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onLikedPostToggle: PropTypes.func.isRequired,
  onDislikedPostToggle: PropTypes.func.isRequired
};

export default Post;
