import PropTypes from 'prop-types';
import shortid from 'shortid';
import {
  useRef
} from 'hooks/hooks';
import styles from './styles.module.scss';

const Checkbox = ({ isChecked, label, onChange }) => {
  const nameId = useRef(shortid.generate());
  return (
    <div className={styles.container}>
      <input
        checked={isChecked}
        className={`${styles.switch} ${styles.pointer}`}
        id={nameId.current}
        onChange={onChange}
        type="checkbox"
      />
      <label className={styles.pointer} htmlFor={nameId.current}>{label}</label>
    </div>
  );
};

Checkbox.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default Checkbox;
