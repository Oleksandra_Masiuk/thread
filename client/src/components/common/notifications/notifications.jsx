import PropTypes from 'prop-types';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { send } from '@emailjs/browser';

import { useEffect } from 'hooks/hooks';
import { ENV, NotificationMessage, SocketEvent } from 'common/enums/enums';
import { userType } from 'common/prop-types/prop-types';

const socket = io(ENV.SOCKET_URL);

const Notifications = ({
  user,
  onPostApply,
  onPostApplyDelete,
  onPostApplyUpdate }) => {
  useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id, email } = user;
    socket.emit(SocketEvent.CREATE_ROOM, id);
    socket.on(SocketEvent.LIKE, (_, username, postId) => {
      NotificationManager.info(NotificationMessage.LIKED_POST);
      const link = `${window.location.origin}/share/${postId}`;
      send(
        ENV.SERVICE_ID,
        ENV.GENERAL_TEMPLATE_ID,
        {
          text: `Your post was liked by ${username}. Post: ${link}`,
          to_email: email
        },
        ENV.USER_ID
      );
    });
    socket.on(SocketEvent.DISLIKE, (_, username, postId) => {
      NotificationManager.info(NotificationMessage.DISLIKED_POST);
      const link = `${window.location.origin}/share/${postId}`;
      send(
        ENV.SERVICE_ID,
        ENV.GENERAL_TEMPLATE_ID,
        {
          text: `Your post was disliked by ${username}. Post: ${link}`,
          to_email: email
        },
        ENV.USER_ID
      );
    });
    socket.on(SocketEvent.LIKE_COMMENT, () => {
      NotificationManager.info(NotificationMessage.LIKED_COMMENT);
    });
    socket.on(SocketEvent.DISLIKE_COMMENT, () => {
      NotificationManager.info(NotificationMessage.DISLIKED_COMMENT);
    });
    socket.on(SocketEvent.UPDATE_PROFILE, ({ id: userId }) => {
      if (userId === id) {
        NotificationManager.info(NotificationMessage.UPDATE_PROFILE);
      }
    });
    socket.on(SocketEvent.NEW_POST, post => {
      if (post.userId !== id) {
        onPostApply(post.id);
      }
    });
    socket.on(SocketEvent.UPDATE_POST, post => {
      if (post.userId !== id) {
        onPostApplyUpdate(post.id);
      }
    });
    socket.on(SocketEvent.DELETE_POST, post => {
      if (post.userId !== id) {
        onPostApplyDelete(post.id);
      }
    });

    return () => {
      socket.emit(SocketEvent.LEAVE_ROOM, id);
      socket.removeAllListeners();
    };
  }, [user, onPostApply, onPostApplyDelete, onPostApplyUpdate]);

  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired,
  onPostApplyDelete: PropTypes.func.isRequired,
  onPostApplyUpdate: PropTypes.func.isRequired
};

export default Notifications;
