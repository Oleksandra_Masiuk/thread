import PropTypes from 'prop-types';
import { useState, useDispatch, useCallback } from 'hooks/hooks';
import {
  IconName,
  IconSize,
  ButtonType,
  AppRoute,
  ButtonColor,
  ButtonSize
} from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { userType } from 'common/prop-types/prop-types';
import { Button, Icon, Image, NavLink, Input } from 'components/common/common';
import { profileActionCreator } from 'store/actions';

import styles from './styles.module.scss';

const Header = ({ user, onUserLogout }) => {
  const [newStatus, setNewStatus] = useState(user.status);
  const [isStatusUpdating, setIsStatusUpdating] = useState(false);

  const dispatch = useDispatch();
  const onProfileToUpdateToggle = useCallback(
    userId => {
      dispatch(profileActionCreator.setProfileToUpdate(userId));
    },
    [dispatch]
  );

  const onUpdateProfile = useCallback(
    profilePayload => dispatch(profileActionCreator.updateProfile(profilePayload)),
    [dispatch]
  );

  const handleProfileUpdate = async e => {
    e.preventDefault();
    if (user.status === newStatus) return;
    onProfileToUpdateToggle(undefined);
    onUpdateProfile({
      ...user,
      status: newStatus
    });
    setIsStatusUpdating(false);
  };

  const handleProfileToUpdate = () => {
    setIsStatusUpdating(true);
    onProfileToUpdateToggle(user.id);
  };
  return (
    <div className={styles.headerWrp}>
      {user && (
        <div className={styles.wrapper}>
          <NavLink to={AppRoute.ROOT}>
            <div className={styles.userWrapper}>
              <Image
                isCircular
                width="45"
                height="45"
                src={user.image?.link ?? DEFAULT_USER_AVATAR}
                alt="user avatar"
              />
              <span className={styles.username}>{user.username}</span>
            </div>
          </NavLink>
          <div className={styles.user__info}>
            {!isStatusUpdating && (
              <div className={styles.statusWrapper}>
                <span className={styles.status}>
                  {user.status || 'Add your status'}
                </span>
                <Button
                  className={styles.headerButton}
                  iconName={IconName.PEN}
                  size={IconSize.SMALL}
                  onClick={handleProfileToUpdate}
                />
              </div>
            )}
            {isStatusUpdating && (
              <form className={styles.inputHeader}>
                <Input
                  value={newStatus || ''}
                  placeholder="Status"
                  onChange={e => setNewStatus(e.target.value)}
                />
                <div className={styles.buttons}>
                  <Button
                    size={ButtonSize.SMALL}
                    color={ButtonColor.RED}
                    type={ButtonType.BUTTON}
                    onClick={() => {
                      onProfileToUpdateToggle(undefined);
                      setIsStatusUpdating(false);
                    }}
                  >
                    Cancel
                  </Button>
                  <Button
                    size={ButtonSize.SMALL}
                    color={ButtonColor.TEAL}
                    type={ButtonType.SUBMIT}
                    onClick={handleProfileUpdate}
                  >
                    Update
                  </Button>
                </div>
              </form>
            )}
          </div>
        </div>
      )}
      <div>
        <NavLink to={AppRoute.PROFILE} className={styles.menuBtn}>
          <Icon name={IconName.USER_CIRCLE} size={IconSize.LARGE} />
        </NavLink>
        <Button
          className={`${styles.menuBtn} ${styles.logoutBtn}`}
          onClick={onUserLogout}
          type={ButtonType.BUTTON}
          iconName={IconName.LOG_OUT}
          iconSize={IconSize.LARGE}
          isBasic
        />
      </div>
    </div>
  );
};

Header.propTypes = {
  onUserLogout: PropTypes.func.isRequired,
  user: userType.isRequired
};

export default Header;
