import PropTypes from 'prop-types';
import clsx from 'clsx';

import { IconName } from 'common/enums/enums';
import Icon from '../icon/icon';

import styles from './styles.module.scss';

const IconButton = ({ iconName, label, onClick, iconMargin }) => (
  <button
    className={clsx(
      styles.iconButton,
      iconMargin && styles[`iconButton__${iconMargin}`]
    )}
    type="button"
    onClick={onClick}
  >
    <Icon name={iconName} />
    {label}
  </button>
);

IconButton.propTypes = {
  iconName: PropTypes.oneOf(Object.values(IconName)).isRequired,
  label: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  onClick: PropTypes.func.isRequired,
  iconMargin: PropTypes.string
};

IconButton.defaultProps = {
  label: '',
  iconMargin: ''
};

export default IconButton;
