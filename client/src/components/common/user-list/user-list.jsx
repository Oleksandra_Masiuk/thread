import PropTypes from 'prop-types';

import styles from './styles.module.scss';
import UserItem from './user-item/user-item';

const UserList = ({ reactions, title }) => (
  <div className={styles.wrapper}>
    <p className={styles.title}>{title}</p>
    <ul>
      {reactions.map(({ user: { username, image, status } }) => (
        <UserItem
          key={username}
          username={username}
          image={image}
          status={status}
        />
      ))}
    </ul>
  </div>
);

UserList.propTypes = {
  title: PropTypes.string.isRequired,
  reactions: PropTypes.arrayOf(PropTypes.any).isRequired
};
export default UserList;
