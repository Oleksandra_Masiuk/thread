import PropTypes from 'prop-types';

import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { Image } from 'components/common/common';
import { imageType } from 'common/prop-types/image';
import styles from './styles.module.scss';

const UserItem = ({ image, username, status }) => (
  <li className={styles.user__info}>
    <Image
      isCircular
      width="45"
      height="45"
      src={image?.link ?? DEFAULT_USER_AVATAR}
      alt="user avatar"
    />
    <div className={styles.Wrapper}>
      <span className={styles.username}>{username}</span>
      <span className={styles.status}>{status}</span>
    </div>
  </li>
);

UserItem.propTypes = {
  image: imageType,
  username: PropTypes.string.isRequired,
  status: PropTypes.string
};
UserItem.defaultProps = {
  image: null,
  status: ''
};
export default UserItem;
