import PropTypes from 'prop-types';
import { useCallback, useDispatch, useSelector } from 'hooks/hooks';
import { threadActionCreator } from 'store/actions';
import { Spinner, Post, Modal } from 'components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  sharePost,
  onPostLike,
  onPostUpdate,
  onPostDelete,
  onPostDislike,
  onPostToUpdateToggle,
  onLikedPostToggle,
  onDislikedPostToggle,
  uploadImage
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handleCommentAdd = useCallback(
    commentPayload => dispatch(threadActionCreator.addComment(commentPayload)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const handleCommentUpdate = useCallback(
    commentPayload => dispatch(threadActionCreator.updateComment(commentPayload)),
    [dispatch]
  );

  const handleCommentDelete = useCallback(
    id => dispatch(threadActionCreator.deleteComment(id)),
    [dispatch]
  );

  const handleUpdatedCommentToggle = useCallback(
    id => dispatch(threadActionCreator.setCommentToUpdate(id)),
    [dispatch]
  );

  const handleCommentLike = useCallback(
    id => dispatch(threadActionCreator.likeComment(id)),
    [dispatch]
  );

  const handleCommentDislike = useCallback(
    id => dispatch(threadActionCreator.dislikeComment(id)),
    [dispatch]
  );

  const handleLikedCommentToggle = useCallback(
    id => dispatch(threadActionCreator.setCommentToLike(id)),
    [dispatch]
  );

  const handleDislikedCommentToggle = useCallback(
    id => dispatch(threadActionCreator.setCommentToDislike(id)),
    [dispatch]
  );
  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal isOpen onClose={handleExpandedPostClose}>
      {post ? (
        <>
          <Post
            post={post}
            onPostLike={onPostLike}
            onPostUpdate={onPostUpdate}
            onPostDelete={onPostDelete}
            onPostDislike={onPostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onLikedPostToggle={onLikedPostToggle}
            onDislikedPostToggle={onDislikedPostToggle}
            onPostToUpdateToggle={onPostToUpdateToggle}
            sharePost={sharePost}
            uploadImage={uploadImage}
          />
          <div>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                onCommentToUpdateToggle={handleUpdatedCommentToggle}
                onCommentUpdate={handleCommentUpdate}
                onCommentDelete={handleCommentDelete}
                onCommentLike={handleCommentLike}
                onCommentDislike={handleCommentDislike}
                onLikedCommentToggle={handleLikedCommentToggle}
                onDislikedCommentToggle={handleDislikedCommentToggle}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </div>
        </>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostToUpdateToggle: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onLikedPostToggle: PropTypes.func.isRequired,
  onDislikedPostToggle: PropTypes.func.isRequired
};

export default ExpandedPost;
