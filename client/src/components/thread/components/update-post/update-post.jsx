import PropTypes from 'prop-types';
import { useState } from 'hooks/hooks';
import { postType } from 'common/prop-types/prop-types';
import { Button, TextArea, Image, Message } from 'components/common/common';
import { ButtonColor, ButtonType, IconName } from 'common/enums/enums';

import styles from './styles.module.scss';

const UpdatePost = ({ post, close, onPostUpdate, uploadImage }) => {
  const { body } = post;

  const [newBody, setNewBody] = useState(body);
  const [image, setImage] = useState(post.image);
  const [isUploading, setIsUploading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handlePostUpdate = e => {
    e.preventDefault();
    const hasNotChanged = body === newBody && post.image === image;
    if (!newBody || hasNotChanged) {
      return;
    }
    close();
    onPostUpdate({ ...post, imageId: image?.id ?? null, body: newBody });
  };

  const handleUploadFile = ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;

    uploadImage(file)
      .then(({ id, link }) => {
        setImage({ id, link });
      })
      .catch(() => {
        setErrorMessage('Error while uploading image');
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <form className={styles.form} onSubmit={handlePostUpdate}>
      {errorMessage !== '' && (
        <Message>
          <p>{errorMessage}</p>
        </Message>
      )}

      <TextArea
        name="body"
        value={newBody}
        placeholder="New post description"
        onChange={e => setNewBody(e.target.value)}
      />
      {image?.link && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.link} alt="post_img" />
        </div>
      )}
      <div className={styles.btnWrapper}>
        {image?.link && (
          <Button
            color={ButtonColor.RED}
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
            onClick={() => setImage(null)}
          >
            <label className={styles.btnImgLabel}>Delete image</label>
          </Button>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
        </div>
      </div>
      <div className={styles.buttons}>
        <Button
          className={styles.button}
          isLoading={isUploading}
          isDisabled={isUploading}
          color={ButtonColor.RED}
          type={ButtonType.BUTTON}
          onClick={close}
        >
          Cancel
        </Button>
        <Button
          isLoading={isUploading}
          isDisabled={isUploading}
          className={styles.button}
          color={ButtonColor.TEAL}
          type={ButtonType.SUBMIT}
        >
          Update
        </Button>
      </div>
    </form>
  );
};

UpdatePost.propTypes = {
  post: postType.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;
