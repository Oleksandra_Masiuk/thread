import PropTypes from 'prop-types';
import { send } from '@emailjs/browser';
import {
  useState, useRef,
  useSelector
} from 'hooks/hooks';
import {
  IconName,
  IconColor,
  ButtonColor,
  ButtonType,
  ENV
} from 'common/enums/enums';
import {
  CopyBufferInput,
  Icon,
  Modal,
  Input,
  Message,
  Button
} from 'components/common/common';

import { emailSchema } from 'validation-schemas/validation-schemas';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [isCopied, setIsCopied] = useState(false);
  const [isSent, setIsSent] = useState(false);
  const [email, setEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const input = useRef();

  const { username } = useSelector(state => ({
    username: state.profile.user.username
  }));

  const link = `${window.location.origin}/share/${postId}`;

  const copyToClipboard = ({ target }) => {
    navigator.clipboard.writeText(input.current?.value ?? '');
    target.focus();
    setIsCopied(true);
    setIsSent(false);
  };

  const handlePostShare = async e => {
    e.preventDefault();
    try {
      const res = await emailSchema.validateAsync({ email });
      send(
        ENV.SERVICE_ID,
        ENV.TEMPLATE_SHARED_POST_ID,
        {
          username,
          link,
          to_email: res.email
        },
        ENV.USER_ID
      ).then(
        () => {
          setIsSent(true);
          setIsCopied(false);
        },
        error => {
          setErrorMessage(`Error sending message: ${error.text}`);
        }
      );
    } catch (err) {
      setErrorMessage(err.message);
    }
  };

  return (
    <Modal isOpen isCentered onClose={close}>
      <header className={styles.header}>
        <span>Share Post</span>
        {isCopied && (
          <span>
            <Icon name={IconName.COPY} color={IconColor.GREEN} />
            Copied
          </span>
        )}

        {isSent && (
          <span>
            <Icon name={IconName.CHECK} color={IconColor.GREEN} />
            Sent
          </span>
        )}
      </header>
      <div>
        <CopyBufferInput
          onCopy={copyToClipboard}
          value={`${link}`}
          ref={input}
        />
      </div>
      <form className={styles.form} onSubmit={handlePostShare}>
        {errorMessage !== '' && (
          <Message>
            <p>{errorMessage}</p>
          </Message>
        )}
        <Input
          iconName={IconName.AT}
          placeholder="Send to email"
          type="email"
          value={email}
          onChange={e => {
            setEmail(e.target.value);
            setErrorMessage('');
            setIsSent(false);
          }}
        />
        <Button color={ButtonColor.TEAL} type={ButtonType.SUBMIT}>
          Send
        </Button>
      </form>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.number.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
