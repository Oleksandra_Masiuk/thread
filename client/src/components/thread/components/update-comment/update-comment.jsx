import PropTypes from 'prop-types';
import { useState } from 'hooks/hooks';
import { commentType } from 'common/prop-types/prop-types';
import { Button, TextArea } from 'components/common/common';
import { ButtonColor, ButtonType } from 'common/enums/enums';

import styles from './styles.module.scss';

const UpdateComment = ({ comment, close, onCommentUpdate }) => {
  const { body } = comment;

  const [newBody, setNewBody] = useState(body);

  const handleCommentUpdate = e => {
    e.preventDefault();
    const hasNotChanged = body === newBody;
    if (!newBody || hasNotChanged) {
      return;
    }
    close();
    onCommentUpdate({ ...comment, body: newBody });
  };

  return (
    <form className={styles.form} onSubmit={handleCommentUpdate}>
      <TextArea
        name="body"
        value={newBody}
        placeholder="New comment to post"
        onChange={e => setNewBody(e.target.value)}
      />
      <div className={styles.buttons}>
        <Button
          color={ButtonColor.RED}
          type={ButtonType.BUTTON}
          onClick={close}
        >
          Cancel
        </Button>
        <Button color={ButtonColor.TEAL} type={ButtonType.SUBMIT}>
          Update
        </Button>
      </div>
    </form>
  );
};

UpdateComment.propTypes = {
  comment: commentType.isRequired,
  close: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired
};

export default UpdateComment;
