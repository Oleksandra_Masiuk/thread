import { getFromNowTime } from 'helpers/helpers';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import {
  IconName,
  ButtonType,
  ButtonColor,
  ButtonSize,
  IconMargin
} from 'common/enums/enums';
import { Button, IconButton, UserList, Modal } from 'components/common/common';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { commentType } from 'common/prop-types/prop-types';

import styles from './styles.module.scss';
import UpdateComment from '../update-comment/update-comment';

const Comment = ({
  comment,
  onCommentToUpdateToggle,
  onCommentDelete,
  onCommentUpdate,
  onCommentLike,
  onCommentDislike,
  onLikedCommentToggle,
  onDislikedCommentToggle
}) => {
  const {
    id: cid,
    body,
    createdAt,
    user,
    updatedAt,
    commentLikeCount,
    commentDislikeCount
  } = comment;

  const likes = Number(commentLikeCount);
  const dislikes = Number(commentDislikeCount);

  const date = getFromNowTime(updatedAt);

  const { userId, commentToUpdate, commentToLike, commentToDislike } = useSelector(state => ({
    userId: state.profile.user.id,
    commentToUpdate: state.posts.commentToUpdate,
    commentToLike: state.posts.commentToLike,
    commentToDislike: state.posts.commentToDislike
  }));

  const isUpdating = commentToUpdate && cid === commentToUpdate.id;

  const handleCommentToUpdate = () => onCommentToUpdateToggle(cid);
  const handleCommentDelete = () => onCommentDelete(cid);
  const handleCommentLike = () => onCommentLike(cid);
  const handleCommentDislike = () => onCommentDislike(cid);
  const handleCommentToLike = () => onLikedCommentToggle(cid);
  const handleCommentToDislike = () => onDislikedCommentToggle(cid);

  const { image, username, id, status } = user;
  return (
    <div className={styles.commentWrapper}>
      <div className={styles.comment}>
        <div>
          <img
            className={styles.avatar}
            src={image?.link ?? DEFAULT_USER_AVATAR}
            alt="avatar"
          />
        </div>
        <div>
          <div>
            <span className={styles.author}>{username}</span>
            <span className={styles.date}>
              {updatedAt === createdAt ? 'commented at' : 'updated at'}
              {' - '}
              {date}
            </span>
            {status && <span className={styles.status}>{status}</span>}
          </div>
          {isUpdating ? (
            <UpdateComment
              comment={comment}
              close={() => {
                onCommentToUpdateToggle(undefined);
              }}
              onCommentUpdate={onCommentUpdate}
            />
          ) : (
            <p className={styles.comment__text}>{body}</p>
          )}
        </div>
      </div>
      {userId === id && !isUpdating && (
        <div className={styles.button}>
          <Button
            color={ButtonColor.TEAL}
            onClick={handleCommentToUpdate}
            type={ButtonType.BUTTON}
          >
            Update
          </Button>
          <Button
            color={ButtonColor.RED}
            onClick={handleCommentDelete}
            type={ButtonType.BUTTON}
          >
            Delete
          </Button>
        </div>
      )}

      <div className={styles.extra}>
        <div className={styles.wrapper}>
          <IconButton
            iconName={IconName.THUMBS_UP}
            iconMargin={IconMargin.MEDIUM}
            onClick={handleCommentLike}
          />
          {' '}
          {likes !== 0 && (
            <Button
              size={ButtonSize.MEDIUM}
              color={ButtonColor.TRANSPARENT}
              type={ButtonType.BUTTON}
              onClick={handleCommentToLike}
            >
              {likes}
              <span className={styles.count}>
                {likes === 1 ? 'Like' : 'Likes'}
              </span>
            </Button>
          )}
        </div>
        <div className={styles.wrapper}>
          <IconButton
            iconName={IconName.THUMBS_DOWN}
            iconMargin={IconMargin.MEDIUM}
            onClick={handleCommentDislike}
          />
          {dislikes !== 0 && (
            <Button
              size={ButtonSize.MEDIUM}
              color={ButtonColor.TRANSPARENT}
              type={ButtonType.BUTTON}
              onClick={handleCommentToDislike}
            >
              {dislikes}
              <span className={styles.count}>
                {dislikes === 1 ? 'Dislike' : 'Dislikes'}
              </span>
            </Button>
          )}
        </div>
      </div>

      {commentToLike && (
        <Modal
          size="small"
          isOpen
          isCentered
          onClose={() => {
            onLikedCommentToggle(undefined);
          }}
        >
          <UserList reactions={commentToLike.reactions} title="Liked by" />
        </Modal>
      )}
      {commentToDislike && (
        <Modal
          size="small"
          isOpen
          isCentered
          onClose={() => {
            onDislikedCommentToggle(undefined);
          }}
        >
          <UserList reactions={commentToDislike.reactions} title="Disliked by" />
        </Modal>
      )}
    </div>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentToUpdateToggle: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onCommentDislike: PropTypes.func.isRequired,
  onLikedCommentToggle: PropTypes.func.isRequired,
  onDislikedCommentToggle: PropTypes.func.isRequired
};

export default Comment;
