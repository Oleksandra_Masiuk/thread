import {
  useState,
  useCallback,
  useEffect,
  useDispatch,
  useSelector
} from 'hooks/hooks';
import InfiniteScroll from 'react-infinite-scroll-component';
import { threadActionCreator } from 'store/actions';
import { image as imageService } from 'services/services';
import { Post, Spinner, Checkbox } from 'components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  isOwnPosts: null,
  isLikedPosts: null
};

const Thread = () => {
  const dispatch = useDispatch();
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showOwnLikedPosts, setShowOwnLikedPosts] = useState(false);
  const [showNotOwnPosts, setShowNotOwnPosts] = useState(false);

  const handlePostLike = useCallback(
    id => dispatch(threadActionCreator.likePost(id)),
    [dispatch]
  );

  const handlePostUpdate = useCallback(
    postPayload => dispatch(threadActionCreator.updatePost(postPayload)),
    [dispatch]
  );

  const handlePostDelete = useCallback(
    id => dispatch(threadActionCreator.deletePost(id)),
    [dispatch]
  );

  const handlePostDislike = useCallback(
    id => dispatch(threadActionCreator.dislikePost(id)),
    [dispatch]
  );

  const handleExpandedPostToggle = useCallback(
    id => dispatch(threadActionCreator.toggleExpandedPost(id)),
    [dispatch]
  );

  const handleUpdatedPostToggle = useCallback(
    id => dispatch(threadActionCreator.setPostToUpdate(id)),
    [dispatch]
  );

  const handleLikedPostToggle = useCallback(
    id => dispatch(threadActionCreator.setPostToLike(id)),
    [dispatch]
  );

  const handleDislikedPostToggle = useCallback(
    id => dispatch(threadActionCreator.setPostToDislike(id)),
    [dispatch]
  );

  const handlePostAdd = useCallback(
    postPayload => dispatch(threadActionCreator.createPost(postPayload)),
    [dispatch]
  );

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = useCallback(
    filtersPayload => {
      dispatch(threadActionCreator.loadMorePosts(filtersPayload));
    },
    [dispatch]
  );

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    if (showOwnLikedPosts) {
      setShowOwnLikedPosts(!showOwnLikedPosts);
    }
    if (showNotOwnPosts) {
      setShowNotOwnPosts(!showNotOwnPosts);
    }
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isOwnPosts = true;
    postsFilter.isLikedPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowNotOwnPosts = () => {
    setShowNotOwnPosts(!showNotOwnPosts);
    if (showOwnLikedPosts) {
      setShowOwnLikedPosts(!showOwnLikedPosts);
    }
    if (showOwnPosts) {
      setShowOwnPosts(!showOwnPosts);
    }
    postsFilter.userId = showNotOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isOwnPosts = false;
    postsFilter.isLikedPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnLikedPosts = () => {
    setShowOwnLikedPosts(!showOwnLikedPosts);
    if (showOwnPosts) {
      setShowOwnPosts(!showOwnPosts);
    }
    if (showNotOwnPosts) {
      setShowNotOwnPosts(!showNotOwnPosts);
    }
    postsFilter.userId = showOwnLikedPosts ? undefined : userId;
    postsFilter.from = 0;
    postsFilter.isLikedPosts = true;
    postsFilter.isOwnPosts = null;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = useCallback(() => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  }, [handleMorePostsLoad]);

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  useEffect(() => {
    getMorePosts();
  }, [getMorePosts]);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar_list}>
        <div className={styles.toolbar}>
          <Checkbox
            isChecked={showOwnPosts}
            label="Show only my posts"
            onChange={toggleShowOwnPosts}
          />
        </div>
        <div className={styles.toolbar}>
          <Checkbox
            isChecked={showNotOwnPosts}
            label="Hide my posts"
            onChange={toggleShowNotOwnPosts}
          />
        </div>
        <div className={styles.toolbar}>
          <Checkbox
            isChecked={showOwnLikedPosts}
            label="Show only posts I liked"
            onChange={toggleShowOwnLikedPosts}
          />
        </div>
      </div>
      <InfiniteScroll
        dataLength={posts.length}
        next={getMorePosts}
        scrollThreshold={0.8}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            onPostLike={handlePostLike}
            onPostUpdate={handlePostUpdate}
            onPostDelete={handlePostDelete}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onLikedPostToggle={handleLikedPostToggle}
            onDislikedPostToggle={handleDislikedPostToggle}
            onPostToUpdateToggle={handleUpdatedPostToggle}
            sharePost={sharePost}
            uploadImage={uploadImage}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          onPostLike={handlePostLike}
          onPostUpdate={handlePostUpdate}
          onPostToUpdateToggle={handleUpdatedPostToggle}
          onLikedPostToggle={handleLikedPostToggle}
          onDislikedPostToggle={handleDislikedPostToggle}
          onPostDelete={handlePostDelete}
          onPostDislike={handlePostDislike}
          uploadImage={uploadImage}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
