import { useState, useCallback, useRef, useEffect } from 'hooks/hooks';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import PropTypes from 'prop-types';
import { Image, Input, Button, Message } from 'components/common/common';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { ButtonColor, ButtonType, IconName, ImageSize } from 'common/enums/enums';
import { userType } from 'common/prop-types/prop-types';
import { updateProfileSchema } from 'validation-schemas/validation-schemas';
import { profileActionCreator } from 'store/actions';
import { getCroppedImg, getBlobFromCanvas } from './helper';
import styles from './styles.module.scss';

const UpdateProfile = ({
  close, user,
  uploadImage,
  onUpdateProfile }) => {
  const { username, email, image, status } = user;

  const [newUsername, setNewUsername] = useState(username);
  const [newEmail, setNewEmail] = useState(email);
  const [NewImage, setNewImage] = useState(image);
  const [newStatus, setNewStatus] = useState(status || '');
  const [errorMessage, setErrorMessage] = useState('');
  const [isUploading, setIsUploading] = useState(false);
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({ maxWidth: 200, aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }
    const img = imgRef.current;
    const canvas = previewCanvasRef.current;
    getCroppedImg(img, canvas, completedCrop);
  }, [completedCrop]);

  const validateUpdate = async () => {
    const emailNotChanged = email === newEmail;
    const usernameNotChanged = username === newUsername;
    const imageNotChanged = image === NewImage;
    const statusNotChanged = status === newStatus;
    const hasNotChanged = emailNotChanged
      && usernameNotChanged
      && imageNotChanged
      && statusNotChanged;
    if (hasNotChanged) {
      return false;
    }
    const isUniqueUsername = await profileActionCreator.checkIfUsernameExists(
      newUsername
    );
    if (isUniqueUsername && !usernameNotChanged) {
      setErrorMessage('This username is already is use');
      return false;
    }
    const isUniqueEmail = await profileActionCreator.checkIfEmailExists(
      newEmail
    );
    if (isUniqueEmail && !emailNotChanged) {
      setErrorMessage('This email is already is use');
      return false;
    }
    return true;
  };

  const handleUploadFile = async file => {
    setIsUploading(true);
    try {
      const { id, link } = await uploadImage(file);
      setNewImage({ id, link });
    } catch {
      setErrorMessage('Error while uploading image');
    }
    setIsUploading(false);
  };

  function closeAvatarEdit() {
    setUpImg(null);
    setCompletedCrop(null);
    setCrop(null);
    imgRef.current = null;
    previewCanvasRef.current = null;
  }

  const handleSaveAvatar = async () => {
    try {
      const jpgFile = getBlobFromCanvas(previewCanvasRef.current, completedCrop);
      if (!jpgFile) return;
      await handleUploadFile(jpgFile);
      closeAvatarEdit();
    } catch (err) {
      setErrorMessage(err.message);
    }
  };

  const handleProfileUpdate = async e => {
    e.preventDefault();
    try {
      const validated = await validateUpdate();
      const res = await updateProfileSchema.validateAsync({ email: newEmail, username: newUsername });
      if (!validated) return;
      close();
      onUpdateProfile({
        ...user,
        imageId: NewImage?.id ?? null,
        username: res.username,
        email: res.email,
        status: newStatus
      });
    } catch (err) {
      setErrorMessage(err.message);
    }
  };

  return (
    <form className={styles.profile} onSubmit={handleProfileUpdate}>
      {errorMessage !== '' && (
        <Message>
          <p>{errorMessage}</p>
        </Message>
      )}
      <Input
        iconName={IconName.USER}
        value={newUsername}
        placeholder="Username"
        onChange={e => {
          setNewUsername(e.target.value);
          setErrorMessage('');
        }}
      />
      <Input
        iconName={IconName.AT}
        placeholder="Email"
        type="email"
        value={newEmail}
        onChange={e => {
          setNewEmail(e.target.value);
          setErrorMessage('');
        }}
      />
      <Input
        value={newStatus || ''}
        placeholder="Status"
        onChange={e => setNewStatus(e.target.value)}
      />
      {!upImg && (
        <Image
          alt="profile avatar"
          isCentered
          src={NewImage?.link ?? DEFAULT_USER_AVATAR}
          size={ImageSize.MEDIUM}
          isCircular
        />
      )}
      <ReactCrop
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => {
          const size = c.width > 200 ? 200 : c.width;
          setCrop({ ...c, width: size, height: size });
        }}
        onComplete={c => {
          const size = c.width > 200 ? 200 : c.width;
          setCompletedCrop({ ...c, width: size, height: size });
        }}
      />
      <div className={styles.canvas}>
        <canvas
          ref={previewCanvasRef}
          style={{
            width: Math.round(completedCrop?.width ?? 0),
            height: Math.round(completedCrop?.height ?? 0)
          }}
        />
      </div>
      <div className={styles.btnWrapper}>
        {!upImg && (
          <>
            {NewImage?.link && (
              <Button
                color={ButtonColor.RED}
                iconName={IconName.IMAGE}
                onClick={() => setNewImage(null)}
                isLoading={isUploading}
                isDisabled={isUploading}
              >
                <label className={styles.btnImgLabel}>Delete avatar</label>
              </Button>
            )}
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                iconName={IconName.IMAGE}
                isLoading={isUploading}
                isDisabled={isUploading}
              >
                <label className={styles.btnImgLabel}>
                  Update avatar
                  <input
                    name="image"
                    type="file"
                    accept="image/*"
                    hidden
                    onChange={onSelectFile}
                  />
                </label>
              </Button>
            </div>
          </>
        )}
        {upImg && (
          <>
            <Button
              color={ButtonColor.RED}
              iconName={IconName.IMAGE}
              onClick={() => {
                closeAvatarEdit();
              }}
              isLoading={isUploading}
              isDisabled={isUploading}
            >
              <label className={styles.btnImgLabel}>Cancel</label>
            </Button>
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                iconName={IconName.IMAGE}
                isLoading={isUploading}
                isDisabled={isUploading}
                onClick={handleSaveAvatar}
              >
                Save avatar
              </Button>
            </div>
          </>
        )}
      </div>
      <div className={styles.buttons}>
        {!upImg && (
          <>
            <Button
              color={ButtonColor.RED}
              type={ButtonType.BUTTON}
              onClick={close}
              isLoading={isUploading}
              isDisabled={isUploading}
            >
              Cancel
            </Button>
            <Button
              color={ButtonColor.TEAL}
              type={ButtonType.SUBMIT}
              isLoading={isUploading}
              isDisabled={isUploading}
            >
              Update
            </Button>
          </>
        )}
      </div>
    </form>
  );
};
UpdateProfile.propTypes = {
  uploadImage: PropTypes.func.isRequired,
  user: userType.isRequired,
  onUpdateProfile: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};
export default UpdateProfile;
