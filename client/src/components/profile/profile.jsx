import { useSelector, useCallback, useDispatch, useState } from 'hooks/hooks';

import { Image, Input, Button } from 'components/common/common';
import { image as imageService } from 'services/services';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import {
  ButtonColor,
  ButtonType,
  ImageSize,
  IconName
} from 'common/enums/enums';
import { profileActionCreator } from 'store/actions';
import styles from './styles.module.scss';
import UpdateProfile from './update-profile/update-profile';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();

  const { id, image, username, email, status } = user;
  const [isUserUpdating, setIsUserUpdating] = useState(false);

  const onProfileToUpdateToggle = useCallback(
    userId => {
      dispatch(profileActionCreator.setProfileToUpdate(userId));
    },
    [dispatch]
  );

  const handleProfileToUpdate = () => {
    setIsUserUpdating(true);
    onProfileToUpdateToggle(id);
  };

  const handleProfileUpdate = useCallback(
    profilePayload => dispatch(profileActionCreator.updateProfile(profilePayload)),
    [dispatch]
  );

  const onUpdateProfile = profilePayload => {
    handleProfileUpdate(profilePayload);
    setIsUserUpdating(false);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.profile}>
      {!isUserUpdating ? (
        <>
          <Image
            alt="profile avatar"
            isCentered
            src={image?.link ?? DEFAULT_USER_AVATAR}
            size={ImageSize.MEDIUM}
            isCircular
          />
          <Input
            iconName={IconName.USER}
            placeholder="Username"
            value={username}
            disabled
          />
          <Input
            iconName={IconName.AT}
            placeholder="Email"
            type="email"
            value={email}
            disabled
          />
          <Input placeholder="Status" value={status || ''} disabled />
          <Button
            className={styles.button}
            color={ButtonColor.TEAL}
            type={ButtonType.BUTTON}
            onClick={handleProfileToUpdate}
          >
            Update
          </Button>
        </>
      ) : (
        <UpdateProfile
          onUpdateProfile={onUpdateProfile}
          user={user}
          close={() => {
            onProfileToUpdateToggle(undefined);
            setIsUserUpdating(false);
          }}
          uploadImage={uploadImage}
        />
      )}
    </div>
  );
};

export default Profile;
