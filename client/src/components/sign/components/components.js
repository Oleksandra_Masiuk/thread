import LoginForm from './login-form/login-form';
import RegistrationForm from './registration-form/registration-form';
import ChangePasswordForm from './change-password-form/change-password-form';
import SendEmailForm from './send-email-form/send-email-form';

export { LoginForm, RegistrationForm, ChangePasswordForm, SendEmailForm };
