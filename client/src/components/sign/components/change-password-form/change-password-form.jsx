import { useNavigate } from 'react-router-dom';
import { useState } from 'hooks/hooks';
import {
  ButtonType,
  ButtonSize,
  ButtonColor,
  AppRoute,
  IconName
} from 'common/enums/enums';
import {
  Button,
  Input,
  Segment,
  Message,
  NavLink
} from 'components/common/common';
import PropTypes from 'prop-types';
import { resetPasswordSchema } from 'validation-schemas/validation-schemas';
import styles from './styles.module.scss';

const ChangePasswordForm = ({ changePassword }) => {
  const [password, setPassword] = useState('');
  const [repeatedPassword, setRepeatedPassword] = useState('');
  const [username, setUsername] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const navigate = useNavigate();

  const handleChangePasswordSubmit = async e => {
    e.preventDefault();
    try {
      const res = await resetPasswordSchema.validateAsync({ username, password });
      if (res.password !== repeatedPassword.trim()) {
        setErrorMessage('Passwords do not match');
        return;
      }
      changePassword(res)
        .unwrap()
        .then(() => navigate('/login'))
        .catch(error => {
          setErrorMessage(error.message);
        });
      setErrorMessage('');
    } catch (err) {
      setErrorMessage(err.message);
    }
  };

  return (
    <>
      <h2 className={styles.title}>Change your password</h2>
      <form className={styles.form} onSubmit={handleChangePasswordSubmit}>
        <Segment>
          <div>
            <Input
              iconName={IconName.USER}
              value={username}
              placeholder="Username"
              type="text"
              onChange={e => {
                setUsername(e.target.value);
                setErrorMessage('');
              }}
            />
          </div>
          <div>
            <Input
              type="password"
              placeholder="Password"
              value={password}
              iconName={IconName.LOCK}
              autoComplete="off"
              onChange={e => {
                setPassword(e.target.value);
                setErrorMessage('');
              }}
            />
          </div>
          <div>
            <Input
              type="password"
              placeholder="Repeat password"
              value={repeatedPassword}
              iconName={IconName.LOCK}
              autoComplete="off"
              onChange={e => {
                setRepeatedPassword(e.target.value);
                setErrorMessage('');
              }}
            />
          </div>
          <div>
            <Button
              type={ButtonType.SUBMIT}
              color={ButtonColor.TEAL}
              size={ButtonSize.LARGE}
            >
              Change password
            </Button>
          </div>
        </Segment>
      </form>
      <Message>
        <div>
          <span>New to us?</span>
          <NavLink to={AppRoute.REGISTRATION}>Sign Up</NavLink>
        </div>
      </Message>
      {errorMessage !== '' && (
        <Message>
          <p>{errorMessage}</p>
        </Message>
      )}
    </>
  );
};

ChangePasswordForm.propTypes = {
  changePassword: PropTypes.func.isRequired
};

export default ChangePasswordForm;
