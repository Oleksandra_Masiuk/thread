import { useState } from 'hooks/hooks';
import { send } from '@emailjs/browser';
import {
  ButtonType,
  ButtonColor,
  IconName,
  IconColor,
  ENV
} from 'common/enums/enums';
import {
  Button,
  Input,
  Segment,
  Message,
  Icon
} from 'components/common/common';
import { emailSchema } from 'validation-schemas/validation-schemas';
import styles from './styles.module.scss';

const SendEmailForm = () => {
  const [isSent, setIsSent] = useState(false);
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmitForm = async e => {
    e.preventDefault();
    try {
      const res = await emailSchema.validateAsync({ email });
      const link = `${window.location.origin}/resetPassword`;
      send(
        ENV.SERVICE_ID,
        ENV.GENERAL_TEMPLATE_ID,
        {
          text: `Link to reset your password: ${link}`,
          to_email: res.email
        },
        ENV.USER_ID
      ).then(
        () => {
          setIsSent(true);
        },
        error => {
          setMessage(`Error sending message: ${error.text}`);
        }
      );
    } catch (err) {
      setMessage(err.message);
    }
  };

  return (
    <>
      <h2 className={styles.title}>Enter your email</h2>
      <Segment>
        <form className={styles.form} onSubmit={handleSubmitForm}>
          <Input
            iconName={IconName.AT}
            placeholder="Send to email"
            type="email"
            value={email}
            onChange={e => {
              setEmail(e.target.value);
              setMessage('');
              setIsSent(false);
            }}
          />
          <Button
            type={ButtonType.SUBMIT}
            color={ButtonColor.TEAL}
          >
            Send link
          </Button>
        </form>
        {isSent && (
          <div className={styles.status}>
            <Icon name={IconName.CHECK} color={IconColor.GREEN} />
            <span className={styles.status_text}>
              Link to reset your password was sent to your email
            </span>
          </div>
        )}
        {message !== '' && (
          <Message>
            <p>{message}</p>
          </Message>
        )}
      </Segment>

    </>
  );
};

export default SendEmailForm;
