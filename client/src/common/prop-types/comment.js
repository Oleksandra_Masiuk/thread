import PropTypes from 'prop-types';
import { imageType } from 'common/prop-types/image';

const commentType = PropTypes.exact({
  id: PropTypes.number.isRequired,
  body: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired,
  deletedAt: PropTypes.string,
  postId: PropTypes.number.isRequired,
  userId: PropTypes.number.isRequired,
  commentLikeCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  commentDislikeCount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  user: PropTypes.exact({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    email: PropTypes.string,
    password: PropTypes.string,
    createdAt: PropTypes.string,
    status: PropTypes.string,
    updatedAt: PropTypes.string,
    imageId: PropTypes.number,
    image: imageType
  }).isRequired
});

export { commentType };
