const ButtonColor = {
  TEAL: 'teal',
  BLUE: 'blue',
  RED: 'red',
  TRANSPARENT: 'transparent'
};

export { ButtonColor };
