const ButtonSize = {
  LARGE: 'large',
  SMALL: 'small',
  MEDIUM: 'medium'
};

export { ButtonSize };
