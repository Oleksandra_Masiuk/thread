const IconSize = {
  LARGE: 'lg',
  SMALL: 'sm'
};

export { IconSize };
