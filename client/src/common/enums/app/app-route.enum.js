const AppRoute = {
  ROOT: '/',
  ANY: '*',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  SHARE_$POSTHASH: '/share/:postHash',
  RESET_PASSWORD: '/resetPassword',
  SEND_EMAIL: '/sendEmail'
};

export { AppRoute };
