const NotificationMessage = {
  LIKED_POST: 'Your post was liked!',
  DISLIKED_POST: 'Your post was disliked!',
  UPDATE_PROFILE: 'Your profile was updated!',
  LIKED_COMMENT: 'Your comment was liked!',
  DISLIKED_COMMENT: 'Your comment was disliked!'
};

export { NotificationMessage };
