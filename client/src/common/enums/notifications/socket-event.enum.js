const SocketEvent = {
  CREATE_ROOM: 'createRoom',
  LEAVE_ROOM: 'leaveRoom',
  LIKE: 'like',
  DISLIKE: 'dislike',
  NEW_POST: 'new_post',
  UPDATE_POST: 'update_post',
  DELETE_POST: 'delete_post',
  UPDATE_PROFILE: 'update_profile',
  UPDATE_COMMENT: 'update_comment',
  DELETE_COMMENT: 'delete_comment',
  LIKE_COMMENT: 'like_comment',
  DISLIKE_COMMENT: 'dislike_comment'
};

export { SocketEvent };
