import { HttpMethod, ContentType } from 'common/enums/enums';

class Auth {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  login(payload) {
    return this._http.load(`${this._apiPath}/auth/login`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  registration(payload) {
    return this._http.load(`${this._apiPath}/auth/register`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }

  getCurrentUser() {
    return this._http.load(`${this._apiPath}/auth/user`, {
      method: HttpMethod.GET
    });
  }

  getUser(id) {
    return this._http.load(`${this._apiPath}/auth/user/${id}`, {
      method: HttpMethod.GET
    });
  }

  getUserByEmail(email) {
    return this._http.load(`${this._apiPath}/auth/user/email/${email}`, {
      method: HttpMethod.GET
    });
  }

  getUserByUsername(username) {
    return this._http.load(`${this._apiPath}/auth/user/username/${username}`, {
      method: HttpMethod.GET
    });
  }

  updateUser(payload, id) {
    return this._http.load(`${this._apiPath}/auth/user/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  changePassword(payload) {
    return this._http.load(`${this._apiPath}/auth/password`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload)
    });
  }
}

export { Auth };
