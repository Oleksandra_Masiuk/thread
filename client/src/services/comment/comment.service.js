import { HttpMethod, ContentType } from 'common/enums/enums';

class Comment {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  getCommentWithLikes(id) {
    return this._http.load(`${this._apiPath}/comments/reactions/likes/${id}`, {
      method: HttpMethod.GET
    });
  }

  getCommentWithDislikes(id) {
    return this._http.load(
      `${this._apiPath}/comments/reactions/dislikes/${id}`,
      {
        method: HttpMethod.GET
      }
    );
  }

  addComment(payload) {
    return this._http.load(`${this._apiPath}/comments`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(payload, id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteComment(id) {
    return this._http.load(`${this._apiPath}/comments/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  likeComment(commentId) {
    return this._http.load(`${this._apiPath}/comments/like`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: true
      })
    });
  }

  dislikeComment(commentId) {
    return this._http.load(`${this._apiPath}/comments/dislike`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: false
      })
    });
  }
}

export { Comment };
