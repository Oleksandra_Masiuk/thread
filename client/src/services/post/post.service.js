import { HttpMethod, ContentType } from 'common/enums/enums';

class Post {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  getPostWithLikes(id) {
    return this._http.load(`${this._apiPath}/posts/reactions/likes/${id}`, {
      method: HttpMethod.GET
    });
  }

  getPostWithDislikes(id) {
    return this._http.load(`${this._apiPath}/posts/reactions/dislikes/${id}`, {
      method: HttpMethod.GET
    });
  }

  updatePost(payload, id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deletePost(id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  addPost(payload) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likePost(postId) {
    return this._http.load(`${this._apiPath}/posts/like`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true
      })
    });
  }

  dislikePost(postId) {
    return this._http.load(`${this._apiPath}/posts/dislike`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: false
      })
    });
  }
}

export { Post };
